#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    char hallName[50];
    char movieName[50];
    char tag[5];
    int group;      /* cinema seat group */
    int groupNumber;    /* cinema seat number */
    int width;      /* hall width */
    int height;     /* hall height */
} Hall;
/****************************************************************************************
 *  Gets a line and sends it to the appropriate function
 *
 * @param f             writes in output.txt file FILE pointer
 * @param hallsArray    stored in hall struct matrix
 * @param singleLine    a file line
 * @param numOfHall     number of "CREATEHALL" command
 */
void lineParser(FILE *f,Hall ***hallsArray,char* singleLine,int numOfHall);
/****************************************************************************************
 *  creates a hall matrix and adds hall array
 *
 * @param hallsArray    array of hall matrix
 * @param singleLine
 */
void createHallMatrix(Hall ***hallsArray, char *singleLine);
/****************************************************************************************
 * calculates how many "CREATEHALL" line
 *
 * @param filename
 * @return
 */
int hallCount(char* filename);
/****************************************************************************************
 * takes a line and parse with " "
 *
 * @param singleLine
 * @return
 */
char** lineToArray(char *singleLine);
/****************************************************************************************
 *
 * checks BUYTICKET command
 * if movie name is whitespace ERROR
 * if movie name is wrong ERROR
 *
 * @param f
 * @param hallsArray
 * @param singleLine
 * @param numOfHall
 */
void buyTicket(FILE *f, Hall ***hallsArray, char* singleLine,int numOfHall);
/****************************************************************************************
 * check movie name is in hall array
 *
 * @param hallsArray
 * @param movieName
 * @param numOfHall
 * @return
 */
int checkMovieName(Hall ***hallsArray, char* movieName,int numOfHall);
/****************************************************************************************
 * reads input file and sends a line lineParser() function
 *
 * @param f
 * @param filename
 * @param hallsArray
 * @param numOfHall
 */
void readFile(FILE *f,char *filename,Hall ***hallsArray, int numOfHall);
/****************************************************************************************
 * checks seats are empty
 * example is empty 10 seats side by side
 *
 * @param f
 * @param hallsArray
 * @param movieName
 * @param customerType   Student or FullFare
 * @param seat
 * @param numOfSeats     How many seats
 * @param numOfHall
 * @return
 */
int checkSeats(FILE *f,Hall ***hallsArray, char* movieName, char* customerType, char* seat, int numOfSeats, int numOfHall);
/****************************************************************************************
 *  returns index of array element
 *
 * @param array    alphabet array
 * @param find     What do you find?
 * @return
 */
int findArrayIndex(char array[],char find);
/****************************************************************************************
 * returns 0 if seats are empty
 * returns 1 if seats are not available
 *
 * @param hallsArray
 * @param movieName
 * @param seat
 * @param numOfSeats
 * @param numOfHall
 * @return
 */
int isAllSeatsEmpty(Hall ***hallsArray, char* movieName, char* seat, int numOfSeats, int numOfHall);
/****************************************************************************************
 * checks CANCELTICKET command
 * if movie name is wrong ERROR
 * if movie name is empty ERROR
 * calls cancelControlSeat() function
 *
 * @param f
 * @param hallsArray
 * @param singleLine
 * @param numOfHall
 */
void cancelTicket(FILE *f,Hall ***hallsArray, char* singleLine,int numOfHall);
/****************************************************************************************
 * checks seat is full or empty
 * if returns 1, seat is full you can cancel it
 * if returns 0, seat is empty
 *
 * @param f
 * @param hallsArray
 * @param movieName
 * @param seat
 * @param numOfHall
 * @return
 */
int cancelControlSeat(FILE *f, Hall ***hallsArray, char* movieName, char* seat, int numOfHall);
/****************************************************************************************
 * draws a hall matrix
 *
 * @param f
 * @param hallsArray
 * @param singleLine
 * @param numOfHall
 */
void showHall(FILE *f, Hall ***hallsArray, char* singleLine,int numOfHall);
/****************************************************************************************
 * shows each room sell how many ticket and earns how many TL's
 *
 * @param f
 * @param hallsArray
 * @param numOfHalls
 */
void statistics(FILE *f, Hall ***hallsArray, int numOfHalls);
/****************************************************************************************
 * calculates a integer's numbers of digits
 *
 * @param number
 * @return
 */
int numOfDigits(int number);
/****************************************************************************************
 *  draw the line according to the width of the hall
 *
 * @param f
 * @param height
 * @param width
 */
void printLine(FILE *f,int height, int width);
/****************************************************************************************
 * writes seat groups
 *
 * @param f
 * @param height
 * @param width
 */
void printGroup(FILE *f,int height,int width);
/****************************************************************************************
 * Determines the location of the curtain and draws
 *
 * @param f
 * @param width
 */
void printCurtain(FILE *f, int width);
/****************************************************************************************
 * returns all purchased seats
 *
 * @param seat
 * @param numOfSeats
 * @return
 */
char* getAllSeats(char* seat, int numOfSeats);

#define STUDENT_FARE_PRICE 7;
#define FULL_FARE_PRICE 10;
#define CREATEHALL "CREATEHALL"
#define BUYTICKET "BUYTICKET"
#define CANCELTICKET "CANCELTICKET"
#define SHOWHALL "SHOWHALL"
#define STATISTICS "STATISTICS"

/**************************************   begins main ***************************************/
int main(int argc, char *argv[]){

    int numOfHall = 0;
    FILE *f;
    Hall ***hallsArray;


    numOfHall = hallCount(argv[1]);
    hallsArray = (Hall ***)calloc((size_t) numOfHall, sizeof(Hall**));

    f = fopen("output.txt","w");

    readFile(f,argv[1],hallsArray,numOfHall);

    fclose(f);

    free(hallsArray);
    /*
    free(f);
    */
    return 0;

}

/**************************************  ends main   ****************************************/


void readFile(FILE *f, char *filename,Hall ***hallsArray, int numOfHall){

    FILE *file = fopen ( filename, "r" );
    if ( file != NULL ) {
        char line [ BUFSIZ ];
        while ( fgets ( line, sizeof line, file ) != NULL )
        {
            lineParser(f,hallsArray,line,numOfHall);
        }
        fclose ( file );
    }
    else {
        perror ( filename );
    }
}


void lineParser(FILE *f, Hall ***hallsArray,char* singleLine,int numOfHall){

    char* token;
    char temp[BUFSIZ];
    strcpy(temp,singleLine);
    token = strtok(temp," ");

    if (strcmp(token,CREATEHALL) == 0){
        createHallMatrix(hallsArray,singleLine);
    } else if (strcmp(token,BUYTICKET) == 0){
        buyTicket(f,hallsArray,singleLine,numOfHall);
    } else if (strcmp(token,CANCELTICKET) == 0){
        cancelTicket(f,hallsArray,singleLine,numOfHall);
    } else if (strcmp(token,SHOWHALL) == 0){
        showHall(f,hallsArray,singleLine,numOfHall);
    } else if (strcmp(token,STATISTICS) == 0){
        statistics(f,hallsArray,numOfHall);
    }
}


int hallCount(char* filename){
    int count = 0;
    FILE *file = fopen ( filename, "r" );
    if ( file != NULL )
    {
        char line [ BUFSIZ ];
        while ( fgets ( line, sizeof line, file ) != NULL )
        {
            char* token = strtok(line," ");
            if (strcmp(token,CREATEHALL) == 0){
                count++;
            }
        }
        fclose ( file );
    }
    else
    {
        perror ( filename );
    }
    /*
    free(file);
    */
    return count;
}

void createHallMatrix(Hall ***hallsArray, char *singleLine){

    int counter = 0;
    int flag = 0;
    int width = 0;
    int height = 0;
    int i, j;
    char hallName[50];
    char movieName[50];
    char** lineArray;
    Hall **pHall;


    lineArray = lineToArray(singleLine);

    width = atoi(lineArray[3]);
    height = atoi(lineArray[4]);

    strcpy(hallName,lineArray[1]);
    strcpy(movieName,lineArray[2]);


    pHall = (Hall **)malloc(height * sizeof(Hall *));

    for (i = 0; i < height; i++){
        pHall[i] = (Hall *)malloc(width * sizeof(Hall));
        for(j = 0; j < width; j++){
            strcpy(pHall[i][j].movieName,movieName);
            strcpy(pHall[i][j].hallName,hallName);
            strcpy(pHall[i][j].tag,"e");
            pHall[i][j].group = j;
            pHall[i][j].groupNumber = i + 1;
            pHall[i][j].height = height;
            pHall[i][j].width = width;
        }
    }



    while (flag == 0){
        if (hallsArray[counter] == NULL){
            hallsArray[counter] = pHall;
            flag = 1;
        } else {
            counter++;
        }
    }
    /*
    free(lineArray);
    free(singleLine);
    */
}

int findArrayIndex(char array[],char find){
    int index = -1;
    const char *ptr = strchr(array, find);
    if (ptr){
        index = (int) (ptr - array);
    }
    return index;
}

char** lineToArray(char *singleLine){

    int i = 0;
    char** lineArray = malloc( 10 * sizeof(char*));
    char temp[BUFSIZ];
    char *token;

    strcpy(temp,singleLine);



    token = strtok(temp," ");
    while (token != NULL){
        lineArray[i] = token;
        token = strtok(NULL, " ");
        i++;
    }
    /*
    free(token);
     */
    return lineArray;
}

int numOfDigits(int number){
    int temp = number;
    int count = 0;

    while (temp != 0){
        temp /= 10;
        ++count;
    }
    return count;
}

void buyTicket(FILE *f,Hall ***hallsArray, char* singleLine,int numOfHall){

    char** lineArray;
    char temp[50];
    char* token;

    lineArray = lineToArray(singleLine);
    strcpy(temp,lineArray[1]);
    token = strtok(temp,"\"");

    if(token == NULL){
        printf("%s\n","ERROR: Movie name cannot be empty");
        fprintf(f,"%s\n","ERROR: Movie name cannot be empty");
    }
    else if (checkMovieName(hallsArray,lineArray[1],numOfHall) == 0){
        printf("ERROR: movie name %s is incorrect.\n",token);
        fprintf(f,"ERROR: movie name %s is incorrect.\n",token);
    } else{
        checkSeats(f,hallsArray,lineArray[1],lineArray[3],lineArray[2],atoi(lineArray[4]),numOfHall);
    }
/*
    free(token);
*/
}


int checkMovieName(Hall ***hallsArray, char* movieName,int numOfHall){

    int temp = 0;
    int i;
    for (i = 0; i < numOfHall; i++) {
        if (hallsArray[i] != NULL) {
            if (strcmp(hallsArray[i][0][0].movieName, movieName) == 0) {
                temp = 1;
                break;
            }
        }
    }
    return temp;
}


int checkSeats(FILE *f,Hall ***hallsArray, char* movieName, char* customerType, char* seat, int numOfSeats, int numOfHall){

    int j,k = 0;
    int len;
    int temp = 0;
    int groupNumber = 0;
    int i;
    int index = 0;
    int p;
    char seatNumber[50];
    char hallTemp[50];

    char alphabet[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

    len = strlen(seat);
    for (j = 1; j < len; j++) {
        seatNumber[k] = seat[j];
        k++;
    }
    groupNumber = atoi(seatNumber);

    for (i = 0; i < numOfHall; i++) {
        if (hallsArray[i] != NULL) {
            if (strcmp(hallsArray[i][0][0].movieName, movieName) == 0) {
                index = findArrayIndex(alphabet,seat[0]);
                strcpy(hallTemp,hallsArray[i][0][0].hallName);
                if (index == -1 || hallsArray[i][0][0].height < groupNumber){
                    temp = 1;
                    printf("ERROR: Seat %s is not defined at %s\n",seat,strtok(hallTemp,"\""));
                    fprintf(f,"ERROR: Seat %s is not defined at %s\n",seat,strtok(hallTemp,"\""));
                }
                else if(hallsArray[i][0][0].width < index + numOfSeats - 1){
                    temp = 2;
                    printf("ERROR: There are not enough seats to the right of %s at %s\n",seat,strtok(hallTemp,"\""));
                    fprintf(f,"ERROR: There are not enough seats to the right of %s at %s\n",seat,strtok(hallTemp,"\""));
                }
                else if (isAllSeatsEmpty(hallsArray,movieName,seat,numOfSeats,numOfHall) == 1){
                    temp = 3;
                    printf("ERROR: Specified seat(s) in %s are not available! They have been already taken.\n",strtok(hallTemp,"\""));
                    fprintf(f,"ERROR: Specified seat(s) in %s are not available! They have been already taken.\n",strtok(hallTemp,"\""));
                }
                else if (isAllSeatsEmpty(hallsArray,movieName,seat,numOfSeats,numOfHall) == 0){
                    temp = 4;
                    for (p = index; p < index + numOfSeats; p++){
                        if (strcmp(customerType,"Student") == 0){
                            strcpy(hallsArray[i][groupNumber - 1][p].tag,"s");
                        } else if (strcmp(customerType,"FullFare") == 0){
                            strcpy(hallsArray[i][groupNumber - 1][p].tag,"f");
                        }
                    }
                    if(numOfSeats == 1){
                        printf("%s [%s] Seat(s) %s successfully sold.\n",strtok(hallTemp,"\""),hallsArray[i][0][0].movieName,seat);
                        fprintf(f,"%s [%s] Seat(s) %s successfully sold.\n",strtok(hallTemp,"\""),hallsArray[i][0][0].movieName,seat);

                    } else{
                        printf("%s [%s] Seat(s) %s successfully sold.\n",strtok(hallTemp,"\""),hallsArray[i][0][0].movieName,getAllSeats(seat,numOfSeats));
                        fprintf(f,"%s [%s] Seat(s) %s successfully sold.\n",strtok(hallTemp,"\""),hallsArray[i][0][0].movieName,getAllSeats(seat,numOfSeats));
                    }
                    }
            }
        }
    }
    /*
    free(customerType);
    free(movieName);
    free(seat);
    free(seatNumber);
    free(hallTemp);
    */

    return temp;

}

char* getAllSeats(char* seat, int numOfSeats){

    char alphabet[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    char allSeats[BUFSIZ];
    int i,index;
    char temp[10];

    strcpy(temp,seat);
    strcpy(allSeats,"");
    allSeats[0] = '\0';
    index = findArrayIndex(alphabet,seat[0]);

    for (i = 0; i < numOfSeats; i++) {
        if (i == numOfSeats -1){
            temp[0] = alphabet[index];
            temp[strlen(seat)] = '\0';
            strcat(allSeats,temp);
            index++;
        } else {
            temp[0] = alphabet[index];
            temp[strlen(seat)] = '\0';
            strcat(allSeats,temp);
            strcat(allSeats,",");
            index++;
        }

    }
    return allSeats;
}

int isAllSeatsEmpty(Hall ***hallsArray, char* movieName, char* seat, int numOfSeats, int numOfHall) {

    int temp = 0;
    int i;
    int len;
    int p,k = 0;
    int j;
    int index;
    char seatNumber[50];
    char alphabet[30] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};


    len = strlen(seat);


    for (p = 1; p < len; p++) {
        seatNumber[k] = seat[p];
        k++;
    }
    for (i = 0; i < numOfHall; i++) {
        if (hallsArray[i] != NULL) {
            if (strcmp(hallsArray[i][0][0].movieName, movieName) == 0) {
                index = findArrayIndex(alphabet, seat[0]);
                for (j = index; j < index + numOfSeats; j++){
                    if (strcmp(hallsArray[i][atoi(seatNumber)- 1][j].tag,"e") != 0){
                        temp = 1;
                        break;
                    }
                }
            }
        }
    }


    return temp;
}

void cancelTicket(FILE *f,Hall ***hallsArray, char* singleLine,int numOfHall){

    char** lineArray;
    char temp[50];
    char* token;
    char* tempLine;

    tempLine = strtok(singleLine,"\n");
    lineArray = lineToArray(tempLine);
    strcpy(temp,lineArray[1]);
    token = strtok(temp,"\"");

    if(token == NULL){
        printf("%s\n","ERROR: Movie name cannot be empty");
        fprintf(f,"%s\n","ERROR: Movie name cannot be empty");
    }
    else if (checkMovieName(hallsArray,lineArray[1],numOfHall) == 0){
        printf("ERROR: movie name %s is incorrect.\n",lineArray[1]);
        fprintf(f,"ERROR: movie name %s is incorrect.\n",lineArray[1]);
    } else{
        cancelControlSeat(f,hallsArray,lineArray[1],lineArray[2],numOfHall);
    }
    /*
    free(singleLine);
    free(lineArray);
    free(tempLine);
     */
}

int cancelControlSeat(FILE *f,Hall ***hallsArray, char* movieName, char* seat, int numOfHall){

    int temp = 0;
    int p,k = 0;
    int len;
    int i;
    int seatNum;
    int index,width,height;
    char seatNumber[50];
    char *tempSeat;
    char tempSeat2[50];
    char tempHallName[50];
    char alphabet[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};


    tempSeat = strtok(seat,"\n");
    strcpy(tempSeat2,tempSeat);

    len = strlen(seat);
    for (p = 1; p < len; p++) {
        seatNumber[k] = seat[p];
        k++;
    }
    seatNum = atoi(seatNumber);
    for (i = 0; i < numOfHall; i++){
        if (hallsArray[i] != NULL) {
            if (strcmp(hallsArray[i][0][0].movieName, movieName) == 0) {
                strcpy(tempHallName,hallsArray[i][0][0].hallName);
                index = findArrayIndex(alphabet, seat[0]);
                width = hallsArray[i][0][0].width;
                height = hallsArray[i][0][0].height;
                if (index > width || seatNum > height){
                    printf("ERROR: Seat %s is not defined at %s\n",strtok(tempSeat2,"\r\n"),strtok(tempHallName,"\""));
                    fprintf(f,"ERROR: Seat %s is not defined at %s\n",strtok(tempSeat2,"\r\n"),strtok(tempHallName,"\""));
                }
                else if (strcmp(hallsArray[i][seatNum - 1][index].tag,"e") == 0){
                    printf("ERROR: Seat %s in %s was not sold.\n",strtok(tempSeat2,"\r\n"),strtok(tempHallName,"\""));
                    fprintf(f,"ERROR: Seat %s in %s was not sold.\n",strtok(tempSeat2,"\r\n"),strtok(tempHallName,"\""));
                    temp = 0;
                    break;
                } else{
                    temp = 1;
                    break;
                }
            }
        }
    }

    if (temp == 1){
        for (i = 0; i < numOfHall; i++) {
            if (hallsArray[i] != NULL) {
                if (strcmp(hallsArray[i][0][0].movieName, movieName) == 0) {
                    strcpy(tempHallName,hallsArray[i][0][0].hallName);
                    index = findArrayIndex(alphabet, seat[0]);
                    if (strcmp(hallsArray[i][seatNum - 1][index].tag, "e") != 0) {
                        strcpy(hallsArray[i][seatNum - 1][index].tag,"e");
                        printf("%s [%s] Purchase cancelled. Seat %s is now free.\n",strtok(tempHallName,"\""),hallsArray[i][0][0].movieName,strtok(tempSeat2,"\r\n"));
                        fprintf(f,"%s [%s] Purchase cancelled. Seat %s is now free.\n",strtok(tempHallName,"\""),hallsArray[i][0][0].movieName,strtok(tempSeat2,"\r\n"));
                    }
                }
            }
        }
    }
    /*
    free(tempSeat);
    free(movieName);
    free(tempHallName);
     */
    return temp;
}


void showHall(FILE *f,Hall ***hallsArray, char* singleLine,int numOfHall){

    int i,j,k;
    int width,height;
    char** lineArray;
    char temp[50];
    char* token;
    char *tempTemp;
    char *hallNameToken;
    char hallNameTemp[50];


    tempTemp = strtok(singleLine,"\r\n");
    lineArray = lineToArray(tempTemp);
    strcpy(temp,lineArray[1]);
    token = strtok(temp,"\"");

    if(strcmp(lineArray[1],"\"\"") == 0){
        printf("%s\n","ERROR: Hall name cannot be empty");
        fprintf(f,"%s\n","ERROR: Hall name cannot be empty");
    } else{
        printf("%s sitting plan\n",token);
        fprintf(f,"%s sitting plan\n",token);
        for (i = 0; i < numOfHall; i++) {
            if (hallsArray[i] != NULL) {
                strcpy(hallNameTemp,hallsArray[i][0][0].hallName);
                hallNameToken = strtok(hallNameTemp,"\"");
                if (strcmp(hallNameToken, token) == 0) {
                    height = hallsArray[i][0][0].height;
                    width = hallsArray[i][0][0].width;
                    for (j = 0; j < height; j++){
                        printLine(f,height,width);
                        for (k = 0; k < width; k++){
                            if (k == 0){
                                int diff = numOfDigits(height) - numOfDigits(hallsArray[i][j][k].groupNumber);
                                printf("%d",hallsArray[i][j][k].groupNumber);
                                fprintf(f,"%d",hallsArray[i][j][k].groupNumber);

                                printf("%.*s",diff,"                            ");
                                fprintf(f,"%.*s",diff,"                            ");
                                if (strcmp(hallsArray[i][j][k].tag,"s") == 0){
                                    printf("|s");
                                    fprintf(f,"|s");
                                } else if (strcmp(hallsArray[i][j][k].tag,"f") == 0){
                                    printf("|f");
                                    fprintf(f,"|f");
                                } else{
                                    printf("| ");
                                    fprintf(f,"| ");
                                }
                            }
                            else if (k == width -1){
                                if (strcmp(hallsArray[i][j][k].tag,"s") == 0){
                                    printf("|s|\n");
                                    fprintf(f,"|s|\n");
                                } else if (strcmp(hallsArray[i][j][k].tag,"f") == 0){
                                    printf("|f|\n");
                                    fprintf(f,"|f|\n");
                                } else{
                                    printf("| |\n");
                                    fprintf(f,"| |\n");
                                }
                            } else{
                                if (strcmp(hallsArray[i][j][k].tag,"s") == 0){
                                    printf("|s");
                                    fprintf(f,"|s");
                                } else if (strcmp(hallsArray[i][j][k].tag,"f") == 0){
                                    printf("|f");
                                    fprintf(f,"|f");
                                } else{
                                    printf("| ");
                                    fprintf(f,"| ");
                                }
                            }
                        }
                    }
                    printLine(f,height,width);
                    printGroup(f,height,width);
                    printCurtain(f,width);
                }
            }
        }
    }
/*
    free(singleLine);
    free(lineArray);
    free(token);
    free(hallNameTemp);
    free(tempTemp);
    */
}

void printLine(FILE *f,int height, int width){
    int i,j;
    int size;
    size = width*2+1;
    for (j = 0; j < numOfDigits(height); j++){
        printf(" ");
        fprintf(f," ");
    }
    for (i = 0; i < size; i++){
        if (i == size -1){
            printf("-\n");
            fprintf(f,"-\n");
        } else{
            printf("-");
            fprintf(f,"-");
        }
    }
}

void printGroup(FILE *f,int height,int width){

    int space;
    int i;
    char alphabet[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};


    space = numOfDigits(height);
    printf("%.*s",space + 1,"                             ");
    fprintf(f,"%.*s",space + 1,"                             ");
    for (i = 0; i < width; i++){
        if (i == width - 1){
            printf("%c\n",alphabet[i]);
            fprintf(f,"%c\n",alphabet[i]);
        } else{
            printf("%c ",alphabet[i]);
            fprintf(f,"%c ",alphabet[i]);
        }
    }
}

void printCurtain(FILE *f, int width){
    int size;
    size = (numOfDigits(width) + (width*2) - 13)/2;
    printf("%.*s",size,"                             ");
    fprintf(f,"%.*s",size,"                             ");

    printf("C U R T A I N\n");
    fprintf(f,"C U R T A I N\n");
}

void statistics(FILE *f,Hall ***hallsArray, int numOfHalls){

    int moneyCounter = 0;
    int studentCounter = 0;
    int fullFareCounter = 0;
    int i,j,k;
    int width,height;
    char mvTemp[50];

    printf("Statistics\n");
    fprintf(f,"Statistics\n");




    for (i = 0; i < numOfHalls; i++) {
        if (hallsArray[i] != NULL){
            width = hallsArray[i][0][0].width;
            height = hallsArray[i][0][0].height;
            for (j = 0; j < height; j++){
                for (k = 0; k < width; k++){
                    if (strcmp(hallsArray[i][j][k].tag,"s") == 0){
                        studentCounter++;
                        moneyCounter += STUDENT_FARE_PRICE;
                    } else if (strcmp(hallsArray[i][j][k].tag,"f") == 0){
                        fullFareCounter++;
                        moneyCounter += FULL_FARE_PRICE;
                    }
                }
            }
            strcpy(mvTemp,hallsArray[i][0][0].movieName);
            printf("%s %d student(s), %d full fare(s), sum:%d TL\n",strtok(mvTemp,"\""),studentCounter,fullFareCounter,moneyCounter);
            fprintf(f,"%s %d student(s), %d full fare(s), sum:%d TL\n",strtok(mvTemp,"\""),studentCounter,fullFareCounter,moneyCounter);

            moneyCounter = 0;
            studentCounter = 0;
            fullFareCounter = 0;

        }
    }
}


